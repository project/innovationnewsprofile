
The Innovation News Installation Profile creates a cross-media newspaper. If you
do not wish to utilize the cross-media tools, the site it creates can be used as
simply a news website.

Installation
------------
Install Drupal 6 using this guide - http://drupal.org/getting-started/6/install
- up to and including the page "Create the Database".

Before you run the install script, unpack the
Innovation News profile to Drupal's profile directory (/profiles).

Also unpack the following modules to your site's module directory
(e.g. /sites/all/modules):
CAPTCHA (http://drupal.org/project/captcha)
Content Construction Kit (CCK) (http://drupal.org/project/cck)
Content Templates (http://drupal.org/project/contemplate)
Image (http://drupal.org/project/image)
Innovation News (http://drupal.org/project/innovationnews)
Pathauto (http://drupal.org/project/pathauto)
Token (http://drupal.org/project/token)
Views (http://drupal.org/project/views)
XML to KML (http://drupal.org/project/xmltokml)

Finally, unpack the following theme to your site's theme directory
(e.g. /sites/all/themes):
Toasted (http://drupal.org/project/toasted)

Continue with Drupal's installation guide, selecting "Innovation News" as the
profile.

If you receive a warning that certain modules have not been installed, be aware
that some of the modules which are required are actually subcomponents of the
modules listed above. Below is a summary:
  -CCK contains: "content", "content_permissions", "number", "text", and
                 "optionwidgets"
  -Image contains: "image", "image_attach", and "image_gallery"
  -Innovation News contains: "innovationnews", "editnews", "editionmanager", and
                             "editionviewer"
  -Views contains: "views" and "views_ui"


Usage
-----
After you have installed the Innovation News profile, visit the Control Center
page via the link in the footer. The Control Center page will provide you with
all the instruction you need to run your Innovation News website successfully.


Credits
-------
The Innovation News Installation Profile is developed by the Open Publishing Lab
at the Rochester Institute of Technology. The idea for this platform was
developed by Professor Michael Riordan, Professor Matthew Bernius, and Professor
Patricia Albanese. The software was developed by John Karahalis.

opl.rit.edu
