<?php

/**
 * Implementation of hook_profile_details().
 */
function innovationnewsprofile_profile_details() {
  $description = st('The Innovation News profile creates a web-to-print ' .
                    'newspaper. Innovation News can alternatively be used as ' .
                    'simply a web-based newspaper.');

  return array(
    'name' => 'Innovation News',
    'description' => $description,
  );

} // function innovationnewsprofile_profile_details

/**
 * Implementation of hook_profile_modules().
 */
function innovationnewsprofile_profile_modules() {
  $core_modules = array(
    // Required core modules.
    'block', 'filter', 'node', 'system', 'user',

    // Optional core modules.
    'dblog', 'help', 'menu', /*'search',*/ 'taxonomy', 'trigger', 'update',
    'path',
  );

  $contributed_modules = array(
    // Innovation News modules.
    'innovationnews', 'editnews', 'editionmanager', 'editionviewer', 'editnews',

    // Other contributed modules.
    /* note cck screwing everything up 'content', 'content_permissions', 'number', 'text',*/ 'image',
    'image_attach', 'image_gallery', 'token', 'pathauto', 'captcha',
    'views', 'views_ui',
  );

  return array_merge($core_modules, $contributed_modules);
} // function innovationnewsprofile_profile_modules

/**
 * Implementation of hook_profile_tasks().
 */
function innovationnewsprofile_profile_tasks(&$task, $url) {
//  _innovationnewsprofile_includes();  // Resolve buggy behavior of installer.

  _innovationnewsprofile_modify_settings();
  _innovationnewsprofile_set_permissions();
  _innovationnewsprofile_set_content_types();
  _innovationnewsprofile_set_vocabularies();
  _innovationnewsprofile_modify_blocks();
  _innovationnewsprofile_save_nodes();
  _innovationnewsprofile_modify_menus();

} // function innovationnewsprofile_profile_tasks

/**
 * Include all files which should have been included by various .install files.
 */
function _innovationnewsprofile_includes() {
  include_once('./'. drupal_get_path('module', 'content') .'/content.module');
} // function _innovationnewsprofile_includes()

/**
 * Modify the default settings of Drupal and contributed modules.
 */
function _innovationnewsprofile_modify_settings() {
  global $theme_key;

  // Set frontpage to "home".
  //variable_set('site_frontpage', 'home');

  // Theme related.
  system_initialize_theme_blocks('toasted');
  variable_set('theme_default', 'toasted');
  variable_set('admin_theme', 'toasted');
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
  $theme_key = 'toasted';

  // Set logo and favicon.
  $toasted_settings = variable_get('theme_toasted_settings', array());
  $toasted_path = drupal_get_path('theme', 'toasted');
  $logo_path = $toasted_path . '/innovationnewsprofile/logo.png';
  $toasted_settings['default_logo'] = 0;
  $toasted_settings['logo_path'] = $logo_path;
  $favicon_path = $toasted_path . '/innovationnewsprofile/favicon.ico';
  $toasted_settings['default_favicon'] = 0;
  $toasted_settings['favicon_path'] = $favicon_path;
  variable_set('theme_toasted_settings', $toasted_settings);

  // Innovation News - set news types.
  $news_types = array('article' => 'article', 'editorial' => 'editorial');
  variable_set('innovationnews_news_types', $news_types);

  // Views - disable hover setting.
  variable_set('views_no_hover_links', '1');

  // CAPTCHA - require CAPTCHA for editorial.
  db_query("INSERT INTO {captcha_points} (form_id, module, type) VALUES " .
           "('%s', '%s', '%s')", 'editorial_node_form', 'captcha', 'Math');

  // Pathauto - define aliases for content types.
  variable_set('pathauto_node_article_pattern', 'articles/[title-raw]');
  variable_set('pathauto_node_editorial_pattern', 'editorials/[title-raw]');
  variable_set('pathauto_node_image_pattern', 'images/[title-raw]');
  variable_set('pathauto_node_page_pattern', '[title-raw]');

  // Image - define size settings and maximum upload size.
  $image_sizes = array(
    '_original' => array('width' => '', 'height' => '', 'label' => t('Original'), 'operation' => 'scale', 'link' => 1),
    'thumbnail' => array('width' => 150, 'height' => 150, 'label' => t('Thumbnail'), 'operation' => 'scale', 'link' => 1),
    'preview'   => array('width' => 640, 'height' => 640, 'label' => t('Preview'), 'operation' => 'scale', 'link' => 1),
  );
  variable_set('image_sizes', $image_sizes);
  variable_set('image_max_upload_size', '2048');

  // Action and Trigger - Redirect to 'content-saved' when content is saved.
/*
  db_query("INSERT INTO {actions} VALUES ('%s', '%s', '%s', '%s', '%s')",
           '1', 'system', 'system_goto_action', 'a:1:{s:3:"url";s:13:"content-saved";}',
           'Redirect to URL (content saved)');
  db_query("INSERT INTO {actions_aid} VALUES (%d)", 1);
  db_query("INSERT INTO {trigger_assignments} VALUES ('%s', '%s', '%s', %d)",
           'nodeapi', 'insert', '1', 1);
*/
} // function _innovationnewsprofile_modify_settings

/**
 * Set the roles and permissions that will be used in this profile.
 */
function _innovationnewsprofile_set_permissions() {
  // Define new roles.
  db_query("INSERT INTO {role} (rid, name) VALUES(%d, '%s')", 3, 'administrator');
  db_query("INSERT INTO {role} (rid, name) VALUES(%d, '%s')", 4, 'writer');
  db_query("INSERT INTO {role} (rid, name) VALUES(%d, '%s')", 5, 'editor');
  db_query("INSERT INTO {role} (rid, name) VALUES(%d, '%s')", 6, 'photographer');
  db_query("INSERT INTO {role} (rid, name) VALUES(%d, '%s')", 7, 'manager');

  // Make user 1 an administrator
  db_query("INSERT INTO {users_roles} VALUES (1, 3)");

  // Update "anonymous user" permissions.
  db_query("UPDATE {permission} SET perm = '%s' WHERE rid = %d",
           'access editionviewer, access content, search content, use ' .
           'advanced search, create editorial content', 1);

  // Update "authenticated user" permissions.
  db_query("UPDATE {permission} SET perm = '%s' WHERE rid = %d",
           'access editionviewer, access content, search content, use ' .
           'advanced search, create editorial content', 2);

  // Set administrator permissions (all permissions).
  $all_perm = 'administer blocks, use PHP for block visibility, administer ' .
              'CAPTCHA settings, skip CAPTCHA, access editionmanager, access ' .
              'editnews, administer filters, create images, edit images, ' .
              'edit own images, view original images, administer images, ' .
              'administer innovationnews settings, administer languages, ' .
              'translate interface, administer menu, access content, ' .
              'administer content types, administer nodes, create article ' .
              'content, create editorial content, create page content, ' .
              'delete any article content, delete any editorial content, ' .
              'delete any page content, delete own article content, delete ' .
              'own editorial content, delete own page content, delete ' .
              'revisions, edit any article content, edit any editorial ' .
              'content, edit own page content, revert revisions, view ' .
              'revisions, administer url aliases, create url aliases, ' .
              'administer pathauto, notify of path changes, administer ' .
              'search, search content, use advanced search, access ' .
              'administration pages, access site reports,  administer ' .
              'actions, administer files, administer site configuration, ' .
              'select different theme, administer taxonomy, access user ' .
              'profiles, administer permissions, administer users, change ' .
              'own username, administer views, access all views';
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (3, '%s', 0)",
           $all_perm);

  // Set "writer" permissions.
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (4, '%s', 0)",
           'create article content');

  // Set "editor" permissions.
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (5, '%s', 0)",
           'access editnews, administer nodes, edit any article content, ' .
           'edit any editorial content');

  // Set "photographer" permissions.
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (6, '%s', 0)",
           'create images');

  // Set "manager" permissions.
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (7, '%s', 0)",
           'access editionmanager, access editionviewer');
} // function _innovationnewsprofile_set_permissions

/**
 * Set the content types for this profile.
 */
function _innovationnewsprofile_set_content_types() {
  // Define content types
  $article_description = st('An article. By default only writers can submit' .
                            'articles.');
  $editorial_description = st('An editorial. By default, any visitor can ' .
                              'submit an editorial.');
  $image_description = st('An image or photograph. By default, only a ' .
                          'photographer can submit an image.');
  $types = array (
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('A static page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'module' => 'node',
      'description' => $article_description,
      'custom' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'editorial',
      'name' => st('Editorial'),
      'module' => 'node',
      'description' => $editorial_description,
      'custom' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'image',
      'name' => st('Image'),
      'module' => 'image',
      'description' => $image_description,
      'body_label' => st('Description'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );

  // Save the node types.
  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Disable all workflow options for article and editorial.
  variable_set('node_options_article', array());
  variable_set('node_options_editorial', array());

  // Disable "Promoted to front page" for images.
  variable_set('node_options_image', array('status'));

  // Allow images to be attached to articles.
  variable_set('image_attach_article', TRUE);

} // function _innovationnews_set_content_types

/**
 * Set vocabularies for this profile.
 */
function _innovationnewsprofile_set_vocabularies() {
  $topic_description = st('This vocabulary includes various possible topics ' .
                          'of news stories. While you are not required to ' .
                          'make use of this vocabulary, doing so improves ' .
                          'site organization and allows for easier ' .
                          'navigation.');

  // Create the "Topic" vocabulary
  $vocabulary_topic = array(
    'name' => st('Topic'),
    'description' => $topic_description,
    'multiple' => 0,
    'required' => 0,
    'hierarchy' => 0,
    'relations' => 0,
    'nodes' => array('article' => TRUE),
  );
  taxonomy_save_vocabulary($vocabulary_topic);
  $vid_topic = $vocabulary_topic['vid'];

  // Define the terms of the topic vocabulary.
  $terms_topic = array(
    array(
      'name' => st('Business'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('Health'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('Politics'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('Science'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('Sports'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('Technology'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
    array(
      'name' => st('World'),
      'vid' => $vid_topic,
      'weight' => 0,
      'parent' => 0,
    ),
  );

  // Save the terms of the Topic vocabulary.
  foreach ($terms_topic as $term) {
    taxonomy_save_term($term);
  }
} // function _innovationnewsprofile_set_vocabularies

/**
 * Modify the block settings.
 */
function _innovationnewsprofile_modify_blocks() {
  _block_rehash();  // Fill the DB with default block info for Toasted.

  // Hide "Powered by Drupal".
  db_query("DELETE FROM {blocks} WHERE module = '%s' AND theme = '%s' " .
           "AND region = '%s'", 'system', 'toasted', 'footer');

  // Move "Primary links" to the header.
  db_query("UPDATE {blocks} SET region = '%s', status = %d, weight = %d " .
           "WHERE theme = '%s' AND delta = '%s'", 'header', 1, 0,
           'toasted', 'primary-links');

  // Hide "User login".
  db_query("UPDATE {blocks} SET status = %d, region = '%s' WHERE " .
           "theme = '%s' AND bid = %d AND module = '%s'",
           0, NULL, 'toasted', 5, 'user');

  // Add "Log in".
/*
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '5', 'garland', 0, 0, NULL, 0, 0, 0, NULL, NULL, -1);
*/
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '5', 'toasted', 1, -10, 'footer', 0, 0, 0, NULL, NULL, -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '5', 1);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           5, '<p class="link">' . l('Log in', 'user') . '</p>', 'Log in', 2);

  // Add "Control Center".
/*
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '5', 'garland', 0, 0, NULL, 0, 0, 0, NULL, NULL, -1);
*/
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '6', 'toasted', 1, -9, 'footer', 0, 0, 0, NULL, NULL, -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '6', 3);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '6', 4);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '6', 5);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '6', 6);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '6', 7);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           6, '<p class="link">' . l('Control Center', 'control-center') . '</p>',
           'Control Center', 2);

  // Add "Writer Panel".
  $writer_content = l('Write an Article', 'node/add/article');
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '3', 'toasted', 1, -8, 'left', 0, 0, 0, 'home',
           'Writer Panel', -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '3', 3);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '3', 4);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           3, $writer_content, 'Writer Panel', 2);

  // Add "Photographer Panel".
  $photographer_content = l('Upload an Image', 'node/add/image');
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '4', 'toasted', 1, -7, 'left', 0, 0, 0, 'home',
           'Photographer Panel', -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '4', 3);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '4', 6);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           4, $photographer_content, 'Photographer Panel', 2);

  // Add "Editor Panel".
  $editor_content = l('Edit News', 'editnews');
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '2', 'toasted', 1, -6, 'left', 0, 0, 0, 'home',
           'Editor Panel', -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '2', 3);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '2', 5);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           2, $editor_content, 'Editor Panel', 2);

  // Add "Manager Panel".
  $manager_content = l('Manage Editions', 'editionmanager') .
                     l('View Editions', 'editionviewer');
  db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, " .
           "region, custom, throttle, visibility, pages, title, cache) " .
           "VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s', %d)",
           'block', '1', 'toasted', 1, -5, 'left', 0, 0, 0, 'home',
           'Manager Panel', -1);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '1', 3);
  db_query("INSERT INTO {blocks_roles} VALUES ('%s', '%s', %d)",
           'block', '1', 7);
  db_query("INSERT INTO {boxes} VALUES (%d, '%s', '%s', %d)",
           1, $manager_content, 'Manager Panel', 2);
} // function _innovationnewsprofile_modify_blocks

/**
 * Save nodes which will be used in the profile.
 */
function _innovationnewsprofile_save_nodes() {
  // Determine the username of user 1.
  $user_1 = user_load(array('uid' => 1));
  $user_1_name = $user_1->name;

  $content_saved_body = st('The content has been saved.');
  $content_saved = array (
    'type' => 'page',
    'language' => 'en',
    'uid' => 1,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
    'moderate' => 0,
    'sticky' => 0,
    'tnid' => 0,
    'translate' => 0,
    'vid' => 2,
    'revision_uid' => 1,
    'title' => st('Content Saved'),
    'body' => $content_saved_body,
    'teaser' => node_teaser($content_saved_body),
    'format' => 1,
    'name' => $user_1_name,
  );
  $content_saved = (object) $content_saved;
  node_save($content_saved);

  // Save the "Control Center" page.
  $control_center_body = st("This is the Control Center.\n\nMore information " .
                            "will be added later.");
  $control_center = array (
    'type' => 'page',
    'language' => 'en',
    'uid' => 1,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
    'moderate' => 0,
    'sticky' => 0,
    'tnid' => 0,
    'translate' => 0,
    'vid' => 1,
    'revision_uid' => 1,
    'title' => st('Control Center'),
    'body' => $control_center_body,
    'teaser' => node_teaser($control_center_body),
    'format' => 1,
    'name' => $user_1_name,
  );
  $control_center = (object) $control_center;
  node_save($control_center);

} // function _innovationnewsprofile_save_nodes

/**
 * Add and delete items from the menus.
 */
function _innovationnewsprofile_modify_menus() {
  $menu_all_stories = array(
    'menu_name' => 'primary-links',
    'link_title' => st('All Stories'),
    'link_path' => 'stories',
    'weight' => 1,
  );
  // Add items to the Primary Links menu.
  $menu_write_editorial = array(
    'menu_name' => 'primary-links',
    'link_title' => st('Write an Editorial'),
    'link_path' => 'node/add/editorial',
    'weight' => 2,
  );
  menu_link_save($menu_write_editorial);
  menu_link_save($menu_all_stories);

  // Add a link to each taxonomy term to the Primary Links menu.
  $menu_taxonomy = array(
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Business'),
      'link_path' => 'taxonomy/term/1',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Health'),
      'link_path' => 'taxonomy/term/2',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Politics'),
      'link_path' => 'taxonomy/term/3',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Science'),
      'link_path' => 'taxonomy/term/4',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Sports'),
      'link_path' => 'taxonomy/term/5',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('Technology'),
      'link_path' => 'taxonomy/term/6',
    ),
    array(
      'menu_name' => 'primary-links',
      'link_title' => st('World'),
      'link_path' => 'taxonomy/term/7',
    ),
  );
  foreach ($menu_taxonomy as $item) {
    menu_link_save($item);
  }

  // Delete module links.
  // Note not working yet, don't know why.
  menu_link_delete(NULL, 'editnews');
  menu_link_delete(NULL, 'editionmanager');
  menu_link_delete(NULL, 'editionviewer');
} // function _innovationnewsprofile_modify_menus
